#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "rs232/rs232.h"
#include "file_utils.h"
#include "everdrive.h"
#include <errno.h>

#define EDC_CHECK	"*T"
#define EDC_LOADROM	"*g"
#define EDC_RUN_MD	"*rm"
#define EDC_RUN_SMS	"*rs"
#define EDC_RUN_CD	"*rc"
#define EDC_RUN_M10	"*rM"
#define EDC_RUN_SSF	"*rS"
#define EDC_ACK		'k'
#define EDC_DACK	'd'	//data ack


#define TRUE	1
#define FALSE	0

static int ed_error = EDR_NONE;


static int openPort(int portID, int baudrate)
{
	if (comOpen(portID, baudrate) == 0)
	{
		return -1;
	}
#ifdef DEBUG
	printf("\tPort %d opened\n", portID);
#endif 

	return portID;
}
static inline void closePort(int portID)
{
	comClose(portID);
}
	
static int getPort(int baudrate)
{
	int i;
	int nbPort = comGetNoPorts();
	everdrive testED;
	testED.baudrate = baudrate;
	
	for (i = 0; i < nbPort; i++)
	{
		if (openPort(i, baudrate) == -1)	continue;


		testED.port = i;
		if (ed_isConnected(&testED))	return i;
	}
	
	return -1;
}

char sendData(everdrive *ed,  char *data, int offset, int len){
	int block_len = 0x8000;// len % 0x10000 == 0 ? 0x10000 : 0x8000;
	for (int i = 0; i < len; i += block_len)
	{
		if (!ed_rawWrite(ed, data + i + offset, block_len)){
			//see ed_error
#ifdef DEBUG
			printf("Error sending block %d/%d %d\n", i/block_len, len/block_len, errno);
#endif
			return FALSE;
		}	
	}

	char buffer[1];
	if (ed_rawRead(ed, buffer, 1) <= 0){
		//see ed_error
#ifdef DEBUG
		printf("Everdrive didn't receive data\n");
#endif
		return FALSE;
	}	
#ifdef DEBUG
		printf("Received %c\n", buffer[0]);
#endif
    return (buffer[0] == EDC_DACK);
}

char ed_open(everdrive *ed, int port, const char * portName, int baudrate)
{
	if (baudrate == DEFAULT_BAUDRATE)	baudrate=115200;
	
	if (comGetNoPorts() == 0)	comEnumerate();
	
	if (portName != NULL)
	{
		port = comFindPort(portName);
		if (port == -1)
		{
#ifdef DEBUG
			printf("%s not found\n", portName);
#endif
			ed_error = EDR_INVALID_PORT;
			return FALSE;
		}
#ifdef DEBUG
		printf("%s found\n", portName);
#endif
	}
	if (port == AUTODETECT_PORT)
	{
		port = getPort(baudrate);
		if ( port == -1)
		{
#ifdef DEBUG
			printf("No device auto detected\n");
#endif
			ed_error = EDR_DEVICE_NOT_FOUND;
			return FALSE;
		}
	}
	
	
	if ( openPort(port, baudrate) == -1)
	{
		ed_error = EDR_INVALID_PORT;
		return FALSE;
	}

	ed->port = port;
	if (!ed_isConnected(ed))
	{

		ed->port = AUTODETECT_PORT;
		ed_error = EDR_DEVICE_NOT_FOUND;
		return FALSE;
	}
	
	ed->portName = comGetPortName(port);
	ed->baudrate = baudrate;

	return TRUE;
}

char ed_close(everdrive *ed)
{
	if (ed == NULL)
	{
		ed_error = EDR_NO_DEVICE;
		return FALSE;
	}
	
	if ( ed->port == -1 )
	{
		ed_error = EDR_INVALID_PORT;
		return FALSE;
	}
	closePort(ed->port);
	
	return TRUE;
}

char ed_isConnected(everdrive *ed)
{
	if (!ed_sendCommand(ed, EDC_CHECK))	{
#ifdef DEBUG
		printf("Check command failed\n");
#endif
		return FALSE;
	}
	if (!ed_readACK(ed))	{
#ifdef DEBUG
		printf("ACK not received\n");
#endif
		return FALSE;
	}

	return TRUE;
}

char ed_rawWrite(everdrive *ed,  char * buffer, size_t len)
{
	if (ed == NULL)
	{
		ed_error = EDR_NO_DEVICE;
		return FALSE;
	}
	if ( ed->port == -1 )
	{
		ed_error = EDR_INVALID_PORT;
		return FALSE;
	}

	int nbByteSent;
	do {
		#ifdef DEBUG
			printf("Try to send %ld bytes to everdrive\n", len);
		#endif

		nbByteSent = comWrite(ed->port, buffer, len);
		if ( (nbByteSent <= 0) && (errno != EAGAIN)) {
	#ifdef DEBUG
			printf("Can't send %ld bytes to everdrive\n", len);
	#endif
			ed_error =  EDR_WRITE;
			return FALSE;
		}

		if (nbByteSent > 0){
		buffer += nbByteSent;
		len -= nbByteSent;
		}
		
		
	}while(len >0);

	return TRUE;
}

int ed_rawRead(everdrive *ed,  char * buffer, size_t len)
{
	if (ed == NULL)
	{
		ed_error = EDR_NO_DEVICE;
		return FALSE;
	}
	if ( ed->port == -1 )
	{
		ed_error = EDR_INVALID_PORT;
		return FALSE;
	}

	int size = comReadBlocking(ed->port, buffer, len, 2000);
	if (size <= 0){
#ifdef DEBUG
		printf("No data received\n");
#endif
		ed_error = EDR_READ;
		return FALSE;
	}

	return size;
}

char ed_sendCommand(everdrive *ed,  char * buffer)
{
	if (!ed_rawWrite(ed, buffer, strlen(buffer)))
	{
#ifdef DEBUG
		printf("Command not sent\n");
#endif
		//see ed_error
		return FALSE;
	}
		
	return TRUE;
}

char ed_readACK(everdrive *ed){
	char buffer[1];

	if (ed_rawRead(ed, buffer, 1) <= 0){
		//see ed_error
		return FALSE;
	}	

	return (buffer[0] == EDC_ACK);
}
char ed_readDACK(everdrive *ed){
	char buffer[1];

	if (ed_rawRead(ed, buffer, 1) <= 0){
		//see ed_error
		return FALSE;
	}	

	return (buffer[0] == EDC_DACK);
}

char ed_loadROM(everdrive *ed, char *rom, long size)
{
	if (!ed_sendCommand(ed, EDC_LOADROM)){
		//see ed_error
		return FALSE;
	}

	char buffer[1];
	buffer[0] = size/512/128;
	if (!ed_rawWrite(ed, buffer, 1)){
		//see ed_error
		return FALSE;
	}

	if (!ed_readACK(ed)){
		//see ed_error
		return FALSE;
	}

	// upload rom
	if (!sendData(ed, rom, 0, size))
	{
		//see ed_error
		return FALSE;
	}

	return TRUE;
}

char *runCommands[8] = {
	"", //#define	MODE_UNKNOWN	0}
	EDC_RUN_MD, //#define MODE_MEGADRIVE	1
	EDC_RUN_SMS, //#define MODE_SMS		2
	EDC_RUN_SSF, //#define MODE_SSF		3
	"", //#define MODE_OS_v1		4
	"", //#define MODE_OS_v2		5
	"", //#define MODE_FPGA		6
	EDC_RUN_M10 ///#define MODE_BIGROM		7
};
char ed_launchROM(everdrive *ed, char mode){

	if (runCommands[mode][0] == 0)	return FALSE;

	if (!ed_sendCommand(ed, runCommands[mode])){
		//see ed_error
		return FALSE;
	}

	if (!ed_readACK(ed))
	{
		return FALSE;
	}

	return TRUE;
}

/*
int ed_loadSRAM(everdrive *ed, char *name, unsigned char *rom, long size)
{
	if (ed == NULL)
	{
		ed_error = ED_NO_DEVICE;
		return -1;
	}
	
	return 0;
}
*/

/*
int ed_updateFirmware(everdrive *ed, char *filename)
{
	long fileSize;
	if (ed == NULL)
	{
		ed_error = ED_NO_DEVICE;
		return -1;
	}
	
	if (!file_isValid(filename))
	{
		ed_error = ED_FILE_NOT_FOUND;
		return -1;
	}
	
	fileSize = file_getSize(filename);
	if (fileSize == 0)
	{
		ed_error = ED_INVALID_FILE;
		return -1;
	}
	
	return 0;
}
*/

int ed_getLastErrorID( )
{
	return ed_error;
}

