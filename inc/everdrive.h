#ifndef _LIB_EVERDRIVE_H_
#define _LIB_EVERDRIVE_H_

typedef struct
{
	int port;
	const char *portName;
	int baudrate;
}everdrive;

#define EDR_NONE 				0	
#define EDR_NO_DEVICE			(EDR_NONE+1)
#define EDR_INVALID_PORT		(EDR_NO_DEVICE+1)
#define EDR_DEVICE_NOT_FOUND 	(EDR_INVALID_PORT+1)
#define EDR_UNSUPPORTED 		(EDR_DEVICE_NOT_FOUND+1)
#define EDR_OUTOFMEM 			(EDR_UNSUPPORTED+1)
#define EDR_WRITE				(EDR_OUTOFMEM+1)
#define EDR_READ				(EDR_WRITE+1)

#define AUTODETECT_PORT		-1
#define DEFAULT_BAUDRATE	-1

#define	MODE_UNKNOWN	0
#define MODE_MEGADRIVE	1
#define MODE_SMS		2
#define MODE_SSF		3
#define MODE_OS_v1		4
#define MODE_OS_v2		5
#define MODE_FPGA		6
#define MODE_BIGROM		7

#ifdef __cplusplus
extern "C" {
#endif


char ed_open( everdrive *ed, int port, const char * name, int baudrate );
char ed_close(everdrive *ed);
char ed_isConnected(everdrive *ed);

char ed_rawWrite(everdrive *ed,  char * buffer, size_t len);
int ed_rawRead(everdrive *ed,  char * buffer, size_t len);
char ed_sendCommand(everdrive *ed,  char * buffer);
char ed_readACK(everdrive *ed);

char ed_loadROM(everdrive *ed,  char *rom, long size);
char ed_launchROM(everdrive *ed, char mode);

/*
int ed_loadSRAM(everdrive *ed,  char *rom, long size);
int ed_updateFirmware(everdrive *ed, char *filename);
*/

int ed_getLastErrorID( );

#ifdef __cplusplus
}
#endif

#endif // _LIB_EVERDRIVE_H_