include makerules 

SRCS = $(wildcard $(SRCDIR)$(SEP)*.c)
SRCS += $(wildcard $(SRCDIR)$(SEP)rs232$(SEP)*.c)
OBJS = $(patsubst %.c,%.o,$(SRCS))
OBJ_RELEASE =  $(patsubst $(SRCDIR)$(SEP)%,$(OBJDIR)$(SEP)%,$(OBJS))

all: release
	
release: $(OUTDIR)$(SEP)libeverdrive.a

$(OUTDIR)$(SEP)libeverdrive.a: folders $(OBJ_RELEASE)
	$(AR) $@ $(OBJ_RELEASE)

$(OBJDIR)$(SEP)%.o: $(SRCDIR)$(SEP)%.c
	$(CC) $(CFLAGS) $(INC) -c -o $@ $<


clean:
	-$(RMD) $(OBJDIR) $(HIDE_ERROR)
	-$(RMD) $(OUTDIR) $(HIDE_ERROR)

folders: bin $(OBJDIR) $(OUTDIR)

bin:
	$(MKDIR) $@
	
$(OBJDIR):
	$(MKDIR) $@
	$(MKDIR) $@$(SEP)rs232

$(OUTDIR):
	$(MKDIR) $@

	
.PHONY: all release clean folders
